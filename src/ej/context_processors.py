from django.conf import settings
from django.utils.translation import get_language

def languages_processor(request):
    return {
        'LANGUAGES': settings.LANGUAGES,
        'LANGUAGE_CODE': get_language(),
    }
