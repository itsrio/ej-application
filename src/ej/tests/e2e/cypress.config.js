const { defineConfig } = require("cypress");

module.exports = defineConfig({
  e2e: {
    env: {
      adminCredentials: {
        'email': 'cypressadmin@mail.com',
        'password': 'admin'
      },
      userCredentiails: {
        'name': 'Cypress',
        'email': 'cypressuser@mail.com',
        'password': 'cypress1234',
        'slug': 'cypressusermailcom'
      }
    },
    baseUrl: 'http://localhost:8000',
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
});
