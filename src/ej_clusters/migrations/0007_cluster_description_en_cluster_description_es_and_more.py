from django.db import migrations, models


def copy_original_to_translation_fields(apps, schema_editor):
    # Obtém o modelo atual
    Cluster = apps.get_model("ej_clusters", "Cluster")

    # Itera por todas as instâncias para copiar os valores originais
    for cluster in Cluster.objects.all():
        if cluster.description:  # Se description não for vazio
            cluster.description_pt_br = cluster.description
        if cluster.name:  # Se name não for vazio
            cluster.name_pt_br = cluster.name
        cluster.save()


class Migration(migrations.Migration):

    dependencies = [
        (
            "ej_clusters",
            "0006_stereotype_description_en_stereotype_description_es_and_more",
        ),
    ]

    operations = [
        migrations.AddField(
            model_name="cluster",
            name="description_en",
            field=models.TextField(
                blank=True,
                help_text="How was this cluster conceived?",
                null=True,
                verbose_name="Description",
            ),
        ),
        migrations.AddField(
            model_name="cluster",
            name="description_es",
            field=models.TextField(
                blank=True,
                help_text="How was this cluster conceived?",
                null=True,
                verbose_name="Description",
            ),
        ),
        migrations.AddField(
            model_name="cluster",
            name="description_pt_br",
            field=models.TextField(
                blank=True,
                help_text="How was this cluster conceived?",
                null=True,
                verbose_name="Description",
            ),
        ),
        migrations.AddField(
            model_name="cluster",
            name="name_en",
            field=models.CharField(max_length=64, null=True, verbose_name="Name"),
        ),
        migrations.AddField(
            model_name="cluster",
            name="name_es",
            field=models.CharField(max_length=64, null=True, verbose_name="Name"),
        ),
        migrations.AddField(
            model_name="cluster",
            name="name_pt_br",
            field=models.CharField(max_length=64, null=True, verbose_name="Name"),
        ),
        # Adiciona a operação para copiar os valores originais
        migrations.RunPython(copy_original_to_translation_fields),
    ]
