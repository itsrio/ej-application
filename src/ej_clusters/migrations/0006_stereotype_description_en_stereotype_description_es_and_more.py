from django.db import migrations, models


def copy_original_to_translation_fields(apps, schema_editor):
    # Obtém o modelo atual
    Stereotype = apps.get_model("ej_clusters", "Stereotype")

    # Itera por todas as instâncias para copiar os valores
    for stereotype in Stereotype.objects.all():
        if stereotype.description:  # Se description não for vazio
            stereotype.description_pt_br = stereotype.description
        if stereotype.name:  # Se name não for vazio
            stereotype.name_pt_br = stereotype.name
        stereotype.save()


class Migration(migrations.Migration):

    dependencies = [
        ("ej_clusters", "0005_alter_stereotypevote_choice"),
    ]

    operations = [
        migrations.AddField(
            model_name="stereotype",
            name="description_en",
            field=models.TextField(
                blank=True,
                help_text="Specify a background history, or give hints about the profile this persona wants to capture. This information is optional and is not made public.",
                null=True,
                verbose_name="Description",
            ),
        ),
        migrations.AddField(
            model_name="stereotype",
            name="description_es",
            field=models.TextField(
                blank=True,
                help_text="Specify a background history, or give hints about the profile this persona wants to capture. This information is optional and is not made public.",
                null=True,
                verbose_name="Description",
            ),
        ),
        migrations.AddField(
            model_name="stereotype",
            name="description_pt_br",
            field=models.TextField(
                blank=True,
                help_text="Specify a background history, or give hints about the profile this persona wants to capture. This information is optional and is not made public.",
                null=True,
                verbose_name="Description",
            ),
        ),
        migrations.AddField(
            model_name="stereotype",
            name="name_en",
            field=models.CharField(
                help_text="Public identification of persona.",
                max_length=64,
                null=True,
                verbose_name="Name",
            ),
        ),
        migrations.AddField(
            model_name="stereotype",
            name="name_es",
            field=models.CharField(
                help_text="Public identification of persona.",
                max_length=64,
                null=True,
                verbose_name="Name",
            ),
        ),
        migrations.AddField(
            model_name="stereotype",
            name="name_pt_br",
            field=models.CharField(
                help_text="Public identification of persona.",
                max_length=64,
                null=True,
                verbose_name="Name",
            ),
        ),
        # Operação personalizada para copiar os dados originais
        migrations.RunPython(copy_original_to_translation_fields),
    ]
