from django.contrib import admin
from django.contrib.flatpages.admin import FlatPageAdmin
from django.contrib.flatpages.models import FlatPage
from modeltranslation.admin import TranslationAdmin
from django.contrib.flatpages.admin import FlatPageAdmin
from django.contrib.flatpages.models import FlatPage
from ej_conversations.admin import ConversationAdmin, CommentAdmin
from ej_conversations.models.conversation import Conversation, Comment
from ej_clusters.models.stereotype import Stereotype
from ej_clusters.admin import StereotypeAdmin

class TranslatedFlatPageAdmin(TranslationAdmin, FlatPageAdmin):
    pass

admin.site.unregister(FlatPage)
admin.site.register(FlatPage, TranslatedFlatPageAdmin)


class TranslatedConversationAdmin(TranslationAdmin, ConversationAdmin):
    pass

admin.site.unregister(Conversation)
admin.site.register(Conversation, TranslatedConversationAdmin)



class TranslatedCommentAdmin(TranslationAdmin, CommentAdmin):
    pass

admin.site.unregister(Comment)
admin.site.register(Comment, TranslatedCommentAdmin)

class TranslatedStereotypeAdmin(TranslationAdmin, StereotypeAdmin):
    pass

admin.site.unregister(Stereotype)
admin.site.register(Stereotype, TranslatedStereotypeAdmin)