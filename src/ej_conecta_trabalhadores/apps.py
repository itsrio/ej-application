from django.apps import AppConfig
from django.urls import include, path


class EjConectaTrabalhadoresConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ej_conecta_trabalhadores"

    def ready(self):
        import ej_conversations.views as conversation_views
        import ej_profiles.views as profile_views
        import ej_conversations.roles.conversations as conversation_roles
        import ej_conversations.serializers as conversation_serializer
        from .views import (
            ConversationDetailView,
            ConversationCreateView,
            ConversationEditView,
            ProfileDetailView,
            EditProfileView,
        )
        from .roles import conversation_card
        import ej_conecta_trabalhadores.translation
        conversation_views.ConversationDetailView = ConversationDetailView
        conversation_views.ConversationCreateView = ConversationCreateView
        conversation_views.ConversationEditView = ConversationEditView
        profile_views.DetailView = ProfileDetailView
        profile_views.EditView = EditProfileView
        conversation_roles.conversation_card = conversation_card
        conversation_serializer.conversation_card = conversation_card

    def get_app_urls(self):
        """
        includes new URLs on ej/urls.py when called by get_apps_dynamic_urls method.
        """
        return path("", include("ej_conecta_trabalhadores.urls", namespace="conecta"))
