from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _


def validate_is_numeric(phone_number):
    phone_number = (
        phone_number.replace("(", "").replace(")", "").replace(" ", "").replace("-", "")
    )
    if not phone_number.isnumeric():
        raise ValidationError(_("Invalid phone format."))
    return phone_number
