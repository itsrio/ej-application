from django.urls import path

from .views import (
    ConectaConversationCommentView,
    EndParticipationView,
    RegisterProfileView,
    RegisterView,
    TourView,
    WelcomeView,
)

app_name = "ej_conecta_trabalhadores"
app_url = "conecta"
conversation_url = (
    "boards/<slug:board_slug>/conversations/<int:conversation_id>/<slug:slug>"
)

urlpatterns = [
    path(
        f"{app_url}/register",
        RegisterView.as_view(),
        name="register",
    ),
    path(
        f"{app_url}/{conversation_url}/profile",
        RegisterProfileView.as_view(),
        name="profile",
    ),
    path(
        f"{app_url}/{conversation_url}/participe",
        WelcomeView.as_view(),
        name="welcome",
    ),
    path(
        f"{app_url}/{conversation_url}/encerrar",
        EndParticipationView.as_view(),
        name="end-participation",
    ),
    path(
        f"{app_url}/profile/tour/",
        TourView.as_view(),
        name="tour",
    ),
    path(
        f"{app_url}/profile/tour/?step=<step>",
        TourView.as_view(),
        name="tour",
    ),
    path(
        f"{app_url}/{conversation_url}/comments/new/",
        ConectaConversationCommentView.as_view(),
        name="new-comment",
    ),
]
