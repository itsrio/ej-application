import re
from django.db import models
from django.utils.translation import gettext_lazy as _
from sidekick import lazy, placeholder as this, property as property

from ej_conversations.enums import Choice
from ej_conversations.models import Conversation as CoreConversation
from ej_profiles.models import Profile as DefaultProfile


BOOLEAN_CHOICES = ((True, _("Yes")), (False, _("No")))


class Conversation(CoreConversation):
    custom_fields_limit = models.PositiveIntegerField(
        default=0,
        help_text=_(
            "Configures from which conversation comment the participant will fill a form\
             requesting additional informations (region, sex, age, gender)."
        ),
        verbose_name=_("Additional profile data"),
    )
    n_user_skiped_votes = property(
        this.votes.filter(author=this.for_user, choice=Choice.SKIP).count()
    )
    n_user_final_votes = lazy(this.user_votes.count())

    def next_comment(self, user, random=True):
        """
        Overwrites next_comment EJ behavior

        Returns a random comment that user didn't vote yet.

        If default value is not given, raises a Comment.DoesNotExit exception
        if no comments are available for user.

        :param random: when False, next_comment will return the same comment.
        :type random: bool
        """
        approved_comments = self.approved_comments
        if not user or user.is_anonymous:
            return approved_comments.first()
        return approved_comments.exclude(votes__author=user).first()

    def reaches_custom_fields_limit(self, user):
        """
        Checks if user reaches the limit for custom_fields_limit
        """
        self.set_request(user)
        custom_fields_limit_is_defined = (
            self.custom_fields_limit
            and self.n_user_final_votes == self.custom_fields_limit
        )
        custom_fields_default_limit_is_defined = (
            not self.custom_fields_limit
            and self.n_approved_comments == self.n_user_final_votes
        )
        return custom_fields_limit_is_defined or custom_fields_default_limit_is_defined


class ConectaProfile(DefaultProfile):
    is_app_worker = models.BooleanField(
        blank=True, null=True, verbose_name=_("Do you consider yourself an app worker?")
    )

    current_age = models.PositiveIntegerField(
        blank=True, null=True, verbose_name=_("Age")
    )


def reaches_anonymous_participation_limit(conversation, user) -> bool:
    reaches_anonymous_participation_limit = getattr(
        conversation, "reaches_anonymous_particiption_limit", False
    )

    return (
        reaches_anonymous_participation_limit
        and conversation.reaches_anonymous_particiption_limit(user)
    ) or user.is_anonymous


def reaches_custom_fields_limit(conversation, user) -> bool:
    user_is_anonymous = user.is_anonymous or re.match(r"^anonymoususer-.*", user.email)
    if not user_is_anonymous:
        reaches_custom_fields_limit = getattr(
            conversation, "reaches_custom_fields_limit", False
        )
        return (
            reaches_custom_fields_limit and conversation.reaches_custom_fields_limit(user)
        ) and not hasattr(user.profile, "conectaprofile")
    return False
