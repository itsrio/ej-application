from django.urls import reverse

from ej.roles import with_template
from ej_conversations import models
from ej_conversations.roles.conversations import conversation_user_progress


@with_template(models.Conversation, role="card")
def conversation_card(
    conversation, url=None, request=None, text=None, hidden=None, button_text=""
):
    """
    Render a round card representing a conversation in a list.
    """

    # Non-authenticated users do not have progress
    if request and request.user.is_authenticated:
        progress = conversation_user_progress(conversation, request=request)
    else:
        progress = None

    return {
        "author": conversation.author_name,
        "text": text or conversation.text,
        "progress": progress,
        "hidden": conversation.is_hidden if hidden is None else hidden,
        "url": reverse("conecta:welcome", kwargs=conversation.get_url_kwargs()),
        "tag": conversation.first_tag,
        "n_comments": conversation.n_approved_comments,
        "n_votes": conversation.n_final_votes,
        "n_favorites": conversation.n_favorites,
        "button_text": button_text,
    }
