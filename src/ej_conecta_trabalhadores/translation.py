# translation.py
from modeltranslation.translator import register, TranslationOptions
from django.contrib.flatpages.models import FlatPage
from modeltranslation.translator import register, TranslationOptions
from ej_conversations.models import Conversation, Comment
from .models import Conversation as ExtendedConversations
from ej_clusters.models.cluster import Cluster
from ej_clusters.models.stereotype import Stereotype
from ej_boards.models import Board

@register(FlatPage)
class FlatPageTranslationOptions(TranslationOptions):
    fields = ('title','content',)  # Campos que terão suporte a tradução

@register(Conversation)
class ConversationTranslationOptions(TranslationOptions):
    fields = ('title','text',)
    

@register(Comment)
class CommentTranslationOptions(TranslationOptions):
    fields = ('content','rejection_reason_text',)
    

@register(ExtendedConversations)
class ConversationTranslationOptions(TranslationOptions):
    fields = ()
    
@register(Stereotype)
class StereotypeTranslationOptions(TranslationOptions):
    fields = ('name','description')
    
@register(Cluster)
class ClusterTranslationOptions(TranslationOptions):
    fields = ('name','description')
    
@register(Board)
class BoardTranslationOptions(TranslationOptions):
    fields = ('title','description')