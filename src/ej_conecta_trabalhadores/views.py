from logging import getLogger
import re

from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.db import transaction, IntegrityError
from django.http import Http404, HttpResponse
from django.shortcuts import redirect, render
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import CreateView, DetailView, TemplateView, UpdateView

from ej.decorators import (
    can_acess_list_view,
    can_edit_conversation,
    check_conversation_overdue,
)
from ej_boards.models import Board
from ej_conversations.decorators import create_session_key
from ej_conversations.forms import CommentForm
from ej_conversations.models import Conversation as CoreConversation
from ej_conversations.rules import max_comments_per_conversation
from ej_conversations.utils import handle_detail_vote
from ej_conversations.utils import handle_detail_comment
from ej_conversations.views import ConversationCommentView, ConversationCommonView
from ej_profiles.forms import ProfileForm, ProfileFormProfilePhoto
from ej_profiles.models import Profile
from ej_profiles.views import DetailView as CoreProfileDetailView, EditView
from ej_integrations.utils import get_host_with_schema
from ej_users.models import User

from .decorators import check_custom_field_limit, user_can_post_anonymously
from .forms import (
    ConectaProfileForm,
    EditProfileForm,
    ConversationForm,
    RegisterConversationProfileForm,
    RegisterConversationTermsForm,
    RegisterConversationUserForm,
)
from .models import Conversation, reaches_anonymous_participation_limit


log = getLogger("ej")


class BaseConversationDetailView:
    def get_queryset(self):
        filter_params = {
            "slug": self.kwargs["slug"],
            "id": self.kwargs["conversation_id"],
        }
        custom_conversation_exists = Conversation.objects.filter(**filter_params).exists()
        default_conversation_exists = CoreConversation.objects.filter(
            **filter_params
        ).exists()
        if custom_conversation_exists:
            return Conversation.objects.all()
        if default_conversation_exists:
            return CoreConversation.objects.all()
        raise Http404

    def get_object(self):
        params = {
            "slug": self.kwargs["slug"],
            "id": self.kwargs["conversation_id"],
        }
        custom_conversation_exists = Conversation.objects.filter(**params).exists()
        default_conversation_exists = CoreConversation.objects.filter(**params).exists()
        if custom_conversation_exists:
            return Conversation.objects.get(**params)
        if default_conversation_exists:
            return CoreConversation.objects.get(**params)
        raise Http404

    def _render_register_template(self, request, conversation):
        return render(
            request,
            "ej_users/conecta-register.jinja2",
            self.register_template_context(request, conversation),
        )

    def register_template_context(self, request, conversation):
        if request.user.is_anonymous:
            n_user_final_votes = 1
        else:
            conversation.set_request(request)
            n_user_final_votes = conversation.n_user_final_votes
        return {
            "n_user_final_votes": n_user_final_votes,
            "conversation": conversation,
            "user": request.user,
            "request": request,
            "form_user": RegisterConversationUserForm(data=request.POST),
            "form_profile": RegisterConversationProfileForm(data=request.POST),
            "form_terms": RegisterConversationTermsForm(),
        }


@method_decorator([login_required, can_edit_conversation], name="dispatch")
class ConversationEditView(BaseConversationDetailView, UpdateView):
    """
    Overwrites ej_conversations.views.ConversationEditView.
    The overwriting occurs on apps.py module.
    """

    model = Conversation
    template_name = "ej_conversations/conversation-edit.jinja2"
    form_class = ConversationForm

    def post(self, request, conversation_id, slug, board_slug, *args, **kwargs):
        conversation = self.get_object()
        board = Board.objects.get(slug=board_slug)
        form = self.form_class(request=request, instance=conversation)
        is_promoted = conversation.is_promoted
        if form.is_valid_post():
            # Check if user is not trying to edit the is_promoted status without
            # permission. This is possible since the form sees this field
            # for all users and does not check if the user is authorized to
            # change is value.

            new = form.save(board=board, **kwargs)
            if new.is_promoted != is_promoted:
                new.is_promoted = is_promoted
                new.save()

            # Now we decide the correct redirect page
            page = request.POST.get("next")
            url = self.get_redirect_url(conversation, page)
            return redirect(url)

        return render(request, self.template_name, self.get_context_data())

    def get_redirect_url(self, conversation, page):
        if page == "stereotypes":
            args = conversation.get_url_kwargs()
            return reverse("boards:cluster-stereotype_votes", kwargs=args)
        elif page == "moderate":
            return conversation.patch_url("conversation:moderate")
        elif conversation.is_promoted:
            return conversation.get_absolute_url()
        else:
            return reverse(
                "boards:dataviz-dashboard", kwargs=conversation.get_url_kwargs()
            )

    def get_context_data(self, **kwargs):
        conversation = self.get_object()
        user = self.request.user

        return {
            "conversation": conversation,
            "form": self.form_class(request=self.request, instance=conversation),
            "menu_links": conversation.get_apps_links(),
            "can_publish": user.has_perm("ej_conversations.can_publish_promoted"),
            "board": conversation.board,
        }


@method_decorator([login_required, can_acess_list_view], name="dispatch")
class ConversationCreateView(CreateView):
    """
    Overwrites ej_conversations.views.ConversationCreateView.
    The overwriting occurs on apps.py module.
    """

    template_name = "ej_conversations/conversation-create.jinja2"
    form_class = ConversationForm

    def post(self, request, board_slug, *args, **kwargs):
        form = self.form_class(request=request)
        kwargs["board"] = self.get_board()

        if form.is_valid():
            with transaction.atomic():
                conversation = form.save_comments(self.request.user, **kwargs)

            return redirect(
                reverse(
                    "conecta:welcome",
                    kwargs=conversation.get_url_kwargs(),
                )
            )

        return render(
            request,
            "ej_conversations/conversation-create.jinja2",
            self.get_context_data(),
        )

    def get_context_data(self, **kwargs):
        user = self.request.user
        user_boards = Board.objects.filter(owner=user)

        return {
            "form": self.form_class(request=self.request),
            "board": self.get_board(),
            "user_boards": user_boards,
        }

    def get_board(self) -> Board:
        board_slug = self.kwargs["board_slug"]
        return Board.objects.get(slug=board_slug)


class EndParticipationView(
    BaseConversationDetailView, ConversationCommonView, DetailView
):
    model = Conversation
    template_name = "ej_conversations/includes/end-participation.jinja2"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["user"] = User.get_or_create_from_session(
            context["conversation"], self.request
        )
        return context


class WelcomeView(BaseConversationDetailView, DetailView):
    model = Conversation
    template_name = "ej_conversations/welcome.jinja2"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["host"] = get_host_with_schema(self.request)
        return context

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["host"] = get_host_with_schema(self.request)
        return context


class RegisterView(CreateView, ConversationCommonView):
    template_name = "ej_conversations/comments/card.jinja2"
    next_url = None
    form_class = CommentForm

    def get_object(self):
        query = {"id": self.request.POST.get("conversation_id")}
        conversation_exists = Conversation.objects.filter(**query).exists()
        if conversation_exists:
            return Conversation.objects.get(**query)
        return CoreConversation.objects.get(**query)

    def post(self, request):
        conversation = self.get_object()
        self.form_profile = RegisterConversationProfileForm(data=request.POST)
        self.form_user = RegisterConversationUserForm(data=request.POST)
        self.form_terms = RegisterConversationTermsForm(request=request)
        self.template_name = "ej_users/conecta-register.jinja2"
        user = None

        if self._forms_are_valid():
            data_profile, data_user = (
                self.form_profile.cleaned_data,
                self.form_user.cleaned_data,
            )
            phone, email, agree_with_terms, agree_with_privacy_policy = (
                data_profile["phone_number"],
                data_user["email"],
                True,
                True,
            )

            # On Conecta Trabalhadores, the register form cannot trigger the
            # "email already exists" error.
            # So, we create the user or find it with the given email.
            # The phone_number will be used as password.
            user = self.create_user(
                request,
                email,
                phone,
                agree_with_terms=agree_with_terms,
                agree_with_privacy_policy=agree_with_privacy_policy,
            )
            # if phone field changes between requests, update it.
            user.set_password(phone)
            user.save()

            user = self._authenticate(email, phone)
            self.template_name = "ej_conversations/conversation-detail.jinja2"

        return render(
            request,
            self.template_name,
            self.get_context_data(user, conversation),
        )

    def create_user(
        self, request, email, password, agree_with_terms, agree_with_privacy_policy
    ):
        session_key = request.session.session_key

        try:
            if session_key:
                user = User.objects.create_user_from_session(session_key, email, password)
            else:
                user, _ = User.objects.get_or_create(
                    email=email,
                    defaults={
                        "password": password,
                        "agree_with_terms": agree_with_terms,
                        "agree_with_privacy_policy": agree_with_privacy_policy,
                    },
                )

            log.info(f"user {user} ({email}) successfully created")
        except Exception as e:
            raise IntegrityError(f"{e}: não foi possível cadastrar o usuário!")
        return user

    def get_context_data(self, user, conversation, **kwargs):
        self.form_profile = RegisterConversationProfileForm(data=self.request)
        self.form_user = RegisterConversationUserForm(data=self.request)
        self.form_terms = RegisterConversationTermsForm(data=self.request)

        form = CommentForm(conversation=conversation)
        n_comments, n_user_final_votes, _ = self.get_statistics(conversation, user)
        user_is_anonymous = user.is_anonymous or re.match(
            r"^anonymoususer-.*", user.email
        )

        return {
            "user_can_add_comments": conversation.user_can_add_comment(user, n_comments),
            "user": user,
            "comment": conversation.next_comment_with_id(user, None),
            "conversation": conversation,
            "n_user_final_votes": n_user_final_votes,
            "form_profile": self.form_profile,
            "form_user": self.form_user,
            "form_terms": self.form_terms,
            "form": form,
            "comment_form": self.form_class(conversation=conversation),
            "max_comments": max_comments_per_conversation(),
            "user_can_participate": getattr(
                self.request, "user_can_post_anonymously", False
            )
            or not user_is_anonymous,
            **kwargs,
        }

    def _forms_are_valid(self):
        return (
            self.form_user.is_valid()
            and self.form_profile.is_valid()
            and self.form_terms.is_valid()
        )

    def _authenticate(self, email, phone):
        authenticated_user = auth.authenticate(self.request, email=email, password=phone)
        auth.login(self.request, authenticated_user)
        authenticated_user.profile.phone_number = phone
        authenticated_user.profile.save()
        return authenticated_user


class ConectaConversationCommentView(ConversationCommentView, BaseConversationDetailView):
    template_name = "ej_conversations/comments/add-comment.jinja2"

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data()
        conversation = context["conversation"]
        user = User.get_or_create_from_session(conversation, self.request)
        context["user"] = user
        context["comment"] = conversation.next_comment(user, random=False)
        return context

    @user_can_post_anonymously
    def post(self, request, conversation_id, slug, board_slug, *args, **kwargs):
        conversation = self.get_object()
        if not getattr(request, "user_can_post_anonymously", False):
            return self._render_register_template(request, conversation)
        request.user = User.get_or_create_from_session(conversation, request)
        self.ctx = handle_detail_comment(request, conversation)

        return render(
            request,
            self.template_name,
            self.get_context_data(),
        )


@method_decorator([check_conversation_overdue], name="dispatch")
class ConversationDetailView(
    BaseConversationDetailView, ConversationCommonView, DetailView
):
    """
    Overwrites ej_conversations.views.ConversationDetailView view.
    The overwriting occurs on apps.py module.
    """

    form_class = CommentForm
    template_name = "ej_conversations/conversation-detail.jinja2"

    @user_can_post_anonymously
    def get(self, request, *args, **kwargs):
        if request.GET.get("comment-addition"):
            return render(
                request,
                "ej_conversations/comments/add-comment.jinja2",
                self.get_context_data(),
            )
        if request.GET.get("return"):
            response = HttpResponse()
            response.status_code = 302
            response["HX-Redirect"] = reverse(
                "boards:conversation-detail",
                kwargs=self.get_object().get_url_kwargs(),
            )
            return response
        return super().get(request, *args, **kwargs)

    @user_can_post_anonymously
    @check_custom_field_limit
    def post(self, request, conversation_id, slug, board_slug, *args, **kwargs):
        conversation = self.get_object()
        conversation.set_request(request)
        request.user = User.get_or_create_from_session(conversation, request)
        handle_detail_vote(request)
        context = self.get_context_data()
        if conversation.n_approved_comments == conversation.n_user_final_votes:
            self.template_name = "ej_conversations/comments/conclusion-card.jinja2"
        else:
            self.template_name = "ej_conversations/comments/card.jinja2"
        return render(
            request,
            self.template_name,
            context,
        )

    @create_session_key
    def get_context_data(self, **kwargs):
        conversation: Conversation = self.get_object()
        conversation.set_request(self.request)
        context = super().get_context_data(**kwargs)
        user = User.get_or_create_from_session(conversation, self.request)
        user_can_post_anonymously = getattr(
            self.request, "user_can_post_anonymously", False
        )
        context.update(
            {
                "user": user,
                "login_anchor": f"{reverse('auth:login')}?next={conversation.get_absolute_url()}",
                "host": get_host_with_schema(self.request),
                "user_can_participate": user_can_post_anonymously,
            }
        )
        if not user_can_post_anonymously:
            register_context = self.register_template_context(self.request, conversation)
            context.update(register_context)
        return context


class RegisterProfileView(DetailView):
    template_name = "ej_conversations/profile-fields.jinja2"
    form_class = ConectaProfileForm
    model = Conversation

    def get_context_data(self, **kwargs):
        return {
            "user": self.request.user,
            "conversation": self.get_object(),
            "form": self.form_class(self.request.user),
            "request": self.request,
        }

    def post(self, request, board_slug, conversation_id, slug, **kwargs):
        conversation = Conversation.objects.get(
            id=self.request.POST.get("conversation_id")
        )
        request.user = User.get_or_create_from_session(conversation, request)
        form = self.form_class(request.user, request=request)

        if form.is_valid():
            form.save()
            response = redirect(
                "boards:conversation-detail", **conversation.get_url_kwargs()
            )
            return response

        return render(
            request,
            self.template_name,
            {
                "conversation": conversation,
                "user": request.user,
                "request": request,
                "form": form,
            },
        )


@method_decorator([login_required], name="dispatch")
class ProfileDetailView(CoreProfileDetailView):
    """
    Overwrites ej_profiles.views.DetailView view.
    The overwriting occurs on apps.py module.
    """

    template_name = "ej_profiles/detail.jinja2"

    def get_object(self) -> Profile:
        profile = self.request.user.get_profile()

        if hasattr(profile, "conectaprofile"):
            return profile.conectaprofile
        return profile

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        profile = self.get_object()
        context.update(
            {
                "is_conecta_profile": hasattr(profile, "is_app_worker"),
            }
        )
        return context


class EditProfileView(EditView):
    """
    Overwrites ej_profiles.views.EditView view.
    The overwriting occurs on apps.py module.
    """

    template_name = "ej_profiles/edit.jinja2"
    form_class = EditProfileForm

    def get_object(self) -> Profile:
        profile = self.request.user.get_profile()

        if hasattr(profile, "conectaprofile"):
            return profile.conectaprofile
        return profile

    def get_context_data(self, **kwargs):
        profile = self.get_object()

        return {
            "form": self.form_class(instance=profile, request=self.request),
            "profile": profile,
            "is_conecta_profile": hasattr(profile, "is_app_worker"),
            "user_boards": Board.objects.filter(owner=self.request.user),
        }

    def post(self, request):
        form = self.form_class(instance=self.get_object(), request=self.request)
        form_profile_photo = ProfileFormProfilePhoto(
            request.POST, request.FILES, instance=self.get_object()
        )
        has_changed = False

        if form.is_valid_post():
            form.save()
            has_changed = True

        if form_profile_photo.is_valid():
            form_profile_photo.save()
            has_changed = True

        if has_changed:
            return redirect("/profile/")

        return render(request, self.template_name, self.get_context_data())


class TourView(TemplateView):
    def _get_tour_step_template(self, step: str):
        if not step:
            return "ej_profiles/tour.jinja2"
        return f"ej_profiles/includes/tour-page{step}.jinja2"

    def get(self, *args, **kwargs):
        tour_step = self.request.GET.get("step")
        template_name = self._get_tour_step_template(tour_step)
        return render(self.request, template_name, self.get_context_data())

    def post(self, *args, **kwargs):
        user = self.request.user
        response = HttpResponse()
        if self.request.GET.get("step") in ["skip", "end"]:
            profile = user.get_profile()
            profile.completed_tour = True
            profile.save()
            """
            Tour page depends on HTMX library to make AJAX requests.
            In order to make a redirect with HTMX,
            we need to include HX-Redirect header in the response.
            For more information, access https://htmx.org/reference/.
            """
            response.status_code = 302
            response["HX-Redirect"] = reverse("user:login")
        return response
