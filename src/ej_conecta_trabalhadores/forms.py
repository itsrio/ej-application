from django import forms
from django.contrib.auth import get_user_model
from django.contrib.flatpages.models import FlatPage
from django.core.exceptions import ValidationError
from django.db.models.fields import BLANK_CHOICE_DASH
from django.forms.forms import NON_FIELD_ERRORS
from django.utils.translation import gettext_lazy as _

from ej.forms import EjModelForm
from ej_conversations.forms import ConversationDateWidget
from ej_conversations.models.comment import Comment
from ej_profiles.enums import STATE_CHOICES
from ej_profiles.forms import (
    EDITABLE_FIELDS,
    EXCLUDE_EDITABLE_FIELDS,
    ProfileForm as DefaultProfileForm,
)

from .models import BOOLEAN_CHOICES, ConectaProfile
from .models import Conversation
from .validators import validate_is_numeric

User = get_user_model()


class ConversationForm(EjModelForm):
    """
    Form used to create and edit conversations.
    """

    comments_count = forms.IntegerField(initial=3, required=False)
    tags = forms.CharField(
        label=_("Tags"), help_text=_("Tags, separated by commas."), required=False
    )
    anonymous_votes_limit = forms.IntegerField(initial=0, min_value=0)

    class Meta:
        model = Conversation
        fields = [
            "title",
            "text",
            "is_promoted",
            "anonymous_votes_limit",
            "custom_fields_limit",
            "start_date",
            "end_date",
        ]
        help_texts = {
            "is_promoted": _("Place conversation in the main /conversations/ URL."),
            "is_hidden": _("Mark to make the conversation invisible."),
        }
        widgets = {
            "start_date": ConversationDateWidget,
            "end_date": ConversationDateWidget,
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in ("tags", "text"):
            self.set_placeholder(field, self[field].help_text)
        if self.instance and self.instance.id is not None:
            self.fields["tags"].initial = ", ".join(
                self.instance.tags.values_list("name", flat=True)
            )

    def set_placeholder(self, field, value):
        self.fields[field].widget.attrs["placeholder"] = value

    def save(self, commit=True, board=None, **kwargs):
        if not board:
            raise ValidationError("Board field should not be empty")
        conversation = super().save(commit=False)
        conversation.board = board

        for k, v in kwargs.items():
            setattr(conversation, k, v)

        if commit:
            conversation.save()
            conversation.set_overdue()

            # Save tags on the database
            tags = self.cleaned_data["tags"].split(",")
            conversation.tags.set(tags, clear=True)

        return conversation

    def save_comments(
        self, author, check_limits=True, status=Comment.STATUS.approved, **kwargs
    ):
        """
        Save model, tags and comments.
        """
        conversation = self.save(author=author, **kwargs)

        # Create comments
        kwargs = {"status": status, "check_limits": check_limits}
        n = int(self.data["comments_count"])
        for i in range(n):
            name = f"comment-{i + 1}"
            value = self.data.get(name)
            if value:
                try:
                    conversation.create_comment(author, value, **kwargs)
                # Duplicate or empty comment...
                except ValidationError:
                    pass
        return conversation


class RegisterConversationTermsForm(EjModelForm):
    def __init__(self, *args, **kwargs):
        super(RegisterConversationTermsForm, self).__init__(*args, **kwargs)
        self.fields["agree_with_terms"].required = True
        self.fields["agree_with_privacy_policy"].required = True
        self.terms_content = {
            "agree_with_terms": self._get_flat_page("/usage/"),
            "agree_with_privacy_policy": self._get_flat_page("/privacy-policy/"),
        }

    def _get_flat_page(self, page_url: str):
        try:
            return FlatPage.objects.get(url=page_url).content
        except Exception:
            return ""

    class Meta:
        model = User
        help_texts = {
            "agree_with_terms": "Concordo com os termos de uso",
            "agree_with_privacy_policy": "Concordo com a politica de privacidade",
        }
        fields = ["agree_with_terms", "agree_with_privacy_policy"]


class RegisterConversationUserForm(forms.Form):
    email = forms.EmailField(label=_("email address"))

    def __init__(self, *args, **kwargs):
        super(RegisterConversationUserForm, self).__init__(*args, **kwargs)
        self.fields["email"].widget.attrs["placeholder"] = "seuemail@mail.com"


class RegisterConversationProfileForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(RegisterConversationProfileForm, self).__init__(*args, **kwargs)
        self.fields["phone_number"].widget.attrs["placeholder"] = "(00) 00000 0000"

    phone_number = forms.CharField(
        label=_("phone number with whatsapp"),
        max_length=15,
        validators=[validate_is_numeric],
    )

    def clean_phone_number(self):
        phone_number = validate_is_numeric(self.cleaned_data["phone_number"])
        return phone_number


class ProfileForm(EjModelForm):
    """
    Form used only for creation of ConectaProfile
    """

    is_app_worker = forms.BooleanField(
        required=False,
        label=_("Do you consider yourself an app worker?"),
        widget=forms.RadioSelect(choices=BOOLEAN_CHOICES),
    )

    state = forms.ChoiceField(
        required=False,
        label=_("Which state do you live in?"),
        choices=BLANK_CHOICE_DASH + list(STATE_CHOICES),
    )

    def __init__(self, user, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = user
        self.fields["gender"].required = False
        self.fields["race"].required = False

    def clean(self):
        """
        method used for evaluating if at least one field of the form was filled
        """
        form_data = self.cleaned_data
        if not (
            form_data["gender"]
            or form_data["race"]
            or form_data["current_age"]
            or form_data["is_app_worker"]
            or form_data["state"]
        ):
            self._errors[NON_FIELD_ERRORS] = [_("You need to fill at least one field")]
        return form_data

    def save(self, commit=True, **kwargs):
        phone_number = self.user.profile.phone_number
        result = super().save(
            commit=commit,
            **kwargs,
            profile_ptr_id=self.user.profile.id,
            user_id=self.user.id,
            phone_number=phone_number,
        )
        return result

    class Meta:
        model = ConectaProfile
        fields = [
            "is_app_worker",
            "state",
            "gender",
            "race",
            "current_age",
            "phone_number",
        ]


class ConectaProfileForm(ProfileForm):
    """
    Form used only for creation of ConectaProfile without phone_number field
    """

    class Meta:
        model = ConectaProfile
        fields = ["is_app_worker", "state", "gender", "race", "current_age"]


class EditProfileForm(DefaultProfileForm):
    """
    User profile form
    """

    class Meta:
        model = ConectaProfile
        EDITABLE_FIELDS += ["is_app_worker", "current_age", "phone_number"]

        fields = [
            field for field in EDITABLE_FIELDS if field not in EXCLUDE_EDITABLE_FIELDS
        ]
        widgets = {
            "state": forms.Select(choices=STATE_CHOICES),
        }
