import re
from django.utils.translation import gettext_lazy as _
from django.urls import reverse
from ej_conecta_trabalhadores.models import (
    reaches_anonymous_participation_limit,
    reaches_custom_fields_limit,
)
from ej_users.models import User
from django.http import HttpResponse


def user_can_post_anonymously(func):
    """
    user_can_post_anonymously checks if conversation was configured to accept
    anonymous votes.
    """

    def wrapper(self, request, conversation_id, slug, board_slug, *args, **kwargs):
        conversation = self.get_object()
        request.user = User.get_or_create_from_session(conversation, request)

        request.user_can_post_anonymously = not reaches_anonymous_participation_limit(
            conversation, request.user
        )
        func_return = func(
            self, request, conversation_id, slug, board_slug, *args, **kwargs
        )
        participation_limit = reaches_anonymous_participation_limit(
            conversation, request.user
        )
        if participation_limit and request.POST:
            return self._render_register_template(request, conversation)

        request.user_can_post_anonymously = not participation_limit
        return func_return

    return wrapper


def check_custom_field_limit(func):
    """
    check if user reached the limit for custom fields form to appear
    If so, it renders the custom fields form
    If user already filled profile no form is showed
    """

    def wrapper(self, request, conversation_id, slug, board_slug, *args, **kwargs):
        func_return = func(
            self, request, conversation_id, slug, board_slug, *args, **kwargs
        )
        conversation = self.get_object()
        user = request.user

        if reaches_custom_fields_limit(conversation, user):
            response = HttpResponse()
            response.status_code = 200
            response["HX-Redirect"] = reverse(
                "conecta:profile", kwargs=conversation.get_url_kwargs()
            )
            return response
        return func_return

    return wrapper
