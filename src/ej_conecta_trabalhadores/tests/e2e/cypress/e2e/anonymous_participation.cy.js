describe('Voting as anonymous user', () => {
  let first_conversation_id = "";
  let second_conversation_id = "";
  let first_conversation_url = "";
  let second_conversation_url = "";

  before(() => {
    cy.removesCypressUser()
    cy.registerUser();
    cy.url().then(($url) => {
      if($url == `${Cypress.config('baseUrl')}/profile/tour/`) {
        cy.get('form a[hx-post="/profile/tour/?step=skip"]').click()
      }
    })
    cy.createConversation()
    cy.url().then(($url) => {
      // tokens[1] should be the URL conversation id.
      let tokens = $url.match(/.*conversations\/(\d*)\//);
      if (parseInt(tokens[1])) {
        first_conversation_id = tokens[1];
        first_conversation_url = $url;
      }
    })
    cy.visit("/profile/home/")
    cy.createSecondConversation()
    cy.url().then(($url) => {
      // tokens[1] should be the URL conversation id.
      let tokens = $url.match(/.*conversations\/(\d*)\//);
      if (parseInt(tokens[1])) {
        second_conversation_id = tokens[1];
        second_conversation_url = $url;
      }
    })
    cy.logout();
  });

  it('access conversation as anonymous user', () => {
    cy.visit(first_conversation_url)
    cy.get('.welcome-page-content h1').contains('Olá!')
    cy.get('.welcome-page__buttons div a').first().click()
  })

  it('vote on conversation as anonymous user', () => {
    cy.visit(first_conversation_url)
    cy.get('.welcome-page-content h1').contains('Olá!')
    cy.get('.welcome-page__buttons a').first().click()
    let vote_url = first_conversation_url.replace("/participe", "").replace("/conecta", "") + "/"
    
    cy.intercept('POST', vote_url).as('vote')
    cy.get('.comment-card__button button[value="agree"]').click()
    cy.wait('@vote');

    cy.intercept('POST', vote_url).as('vote')
    cy.get('.comment-card__button button[value="disagree"]').click()
    cy.wait('@vote');

    cy.get('#id_email').type("cypressuser2@mail.com")
    cy.get('#id_phone_number').type("61981178174")
    cy.get('#id_agree_with_terms').click()
    cy.get('#id_agree_with_privacy_policy').click()
    cy.intercept('POST', "/conecta/register").as('registration')
    cy.get('#send-user-information').should('have.value', 'enviar informações')
    cy.get('#send-user-information').click()
    cy.wait("@registration")

    cy.intercept('POST', vote_url).as('vote')
    cy.get('.comment-card__button button[value="disagree"]').click()
    cy.wait('@vote')
    cy.get('.main-header__avatar')

    cy.get(".form-fields label").first().contains("Você se considera um trabalhador de aplicativo?")
    cy.get('[type="radio"]').first().check()
    cy.get('#id_state').select('DF')
    cy.get('#id_gender').select('1')
    cy.get('#id_race').select('1')
    cy.get('#id_current_age').type('27')
    cy.get('#conecta-send-info-accepted').click()
  })

  it('vote on conversation as anonymous without register', () => {
    cy.visit(first_conversation_url)
    cy.get('.welcome-page-content h1').contains('Olá!')
    cy.get('.welcome-page__buttons a').first().click()
    let vote_url = first_conversation_url.replace("/participe", "").replace("/conecta", "") + "/"
    
    cy.intercept('POST', vote_url).as('vote')
    cy.get('.comment-card__button button[value="agree"]').click()
    cy.wait('@vote');

    cy.intercept('POST', vote_url).as('vote')
    cy.get('.comment-card__button button[value="disagree"]').click()
    cy.wait('@vote');

    cy.get('[class*="width-full is-primary register-form__button"]').should('have.value', 'encerrar participação')
    cy.get('[class*="width-full is-primary register-form__button"]').click()
    cy.get('[class*="conecta-finish-vote comment-card"]').contains('Obrigada pela sua participação!')
  })

  it('vote on conversation as anonymous without filling profile informations', () => {
    cy.visit(first_conversation_url)
    cy.get('.welcome-page-content h1').contains('Olá!')
    cy.get('.welcome-page__buttons a').first().click()
    let vote_url = first_conversation_url.replace("/participe", "").replace("/conecta", "") + "/"
    
    cy.intercept('POST', vote_url).as('vote')
    cy.get('.comment-card__button button[value="agree"]').click()
    cy.wait('@vote');

    cy.intercept('POST', vote_url).as('vote')
    cy.get('.comment-card__button button[value="disagree"]').click()
    cy.wait('@vote');

    cy.get('#id_email').type("cypressuser3@mail.com")
    cy.get('#id_phone_number').type("61981198174")
    cy.get('#id_agree_with_terms').click()
    cy.get('#id_agree_with_privacy_policy').click()
    cy.intercept('POST', "/conecta/register").as('registration')
    cy.get('#send-user-information').should('have.value', 'enviar informações')
    cy.get('#send-user-information').click()
    cy.wait("@registration")
    cy.get('.main-header__avatar')

    cy.intercept('POST', vote_url).as('vote')
    cy.get('.comment-card__button button[value="disagree"]').click()
    cy.wait('@vote')
    
    cy.intercept('GET', vote_url).as('deny-profile')
    cy.get(".form-fields label").first().contains("Você se considera um trabalhador de aplicativo?")
    cy.get('[class*="width-full conecta-profile-form__deny-btn"]').should('have.value', 'Não quero responder')
    cy.get('[class*="width-full conecta-profile-form__deny-btn"]').click()
    cy.wait('@deny-profile')
  })


  after(() => {
    cy.logout()
    cy.reload()
    cy.removesIaConversation()
    cy.removesEducationConversation()
    cy.removesCypressUser();
  });


})
