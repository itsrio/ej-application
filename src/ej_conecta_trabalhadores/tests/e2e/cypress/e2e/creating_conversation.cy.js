describe('Managing a conversation', () => {
  let conversation_id = "";

  it('creates conversation', () => {
    cy.loginWithAdmin()
    cy.url().then(($url) => {
      if($url == `${Cypress.config('baseUrl')}/profile/tour/`) {
        cy.get('form a[hx-post="/profile/tour/?step=skip"]').click()
      }
    })
    cy.createConversation()
    cy.url().then(($url) => {
      // tokens[1] should be the URL conversation id.
      let tokens = $url.match(/.*conversations\/(\d*)\//);
      if (parseInt(tokens[1])) {
        conversation_id = tokens[1];
      }
    })
  })

  it('access conversation participation page', () => {
    cy.login()
    cy.visit(`/cypressmailcom/conversations/${conversation_id}/avancos-da-ia-e2e/`)
    cy.get('.conversation-title h1').contains('O que você acha do avanço da inteligência artificial na sociedade moderna?')
  })

  it('add comment to conversation', () => {
    cy.registerUser();
    cy.url().then(($url) => {
      if($url == `${Cypress.config('baseUrl')}/profile/tour/`) {
        cy.get('form a[hx-post="/profile/tour/?step=skip"]').click()
      }
    })
    
    let conversationUrl = `/cypressadminmailcom/conversations/${conversation_id}/avancos-da-ia-e2e/`
    cy.visit(conversationUrl)

    cy.intercept(conversationUrl).as('vote')
    cy.get('.comment-card__button button[value="agree"]').click()
    cy.wait('@vote');

    cy.intercept(conversationUrl).as('vote')
    cy.get('.comment-card__button button[value="disagree"]').click()
    cy.wait('@vote');

    cy.intercept(conversationUrl).as('vote')
    cy.get('.comment-card__button button[value="disagree"]').click()
    cy.wait('@vote')

    cy.get(".form-fields label").first().contains("Você se considera um trabalhador de aplicativo?")
    cy.get('[type="radio"]').first().check()
    cy.get('#id_state').select('DF')
    cy.get('#id_gender').select('1')
    cy.get('#id_race').select('1')
    cy.get('#id_current_age').type('27')
    cy.get('#conecta-send-info-accepted').click()

    cy.intercept(conversationUrl).as('vote')
    cy.get('.comment-card__button button[value="disagree"]').click()
    cy.wait('@vote')
    
    cy.get('#id_content').type("Um comentário do Cypress na conversa de IA")
    cy.get('#add-comment-form button').click()
  })

  after(() => {
    cy.logout(true)
    cy.removesIaConversation()
  });
})
