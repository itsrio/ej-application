from django.db import migrations, models


def copy_original_to_translation_fields(apps, schema_editor):
    # Obtém o modelo atual
    Board = apps.get_model("ej_boards", "Board")

    # Itera por todas as instâncias para copiar os valores originais
    for board in Board.objects.all():
        if board.description:  # Copia description para description_pt_br
            board.description_pt_br = board.description
        if board.title:  # Copia title para title_pt_br
            board.title_pt_br = board.title
        board.save()


class Migration(migrations.Migration):

    dependencies = [
        ("ej_boards", "0006_board_users_favorite"),
    ]

    operations = [
        migrations.AddField(
            model_name="board",
            name="description_en",
            field=models.TextField(blank=True, null=True, verbose_name="Description"),
        ),
        migrations.AddField(
            model_name="board",
            name="description_es",
            field=models.TextField(blank=True, null=True, verbose_name="Description"),
        ),
        migrations.AddField(
            model_name="board",
            name="description_pt_br",
            field=models.TextField(blank=True, null=True, verbose_name="Description"),
        ),
        migrations.AddField(
            model_name="board",
            name="title_en",
            field=models.CharField(max_length=50, null=True, verbose_name="Title"),
        ),
        migrations.AddField(
            model_name="board",
            name="title_es",
            field=models.CharField(max_length=50, null=True, verbose_name="Title"),
        ),
        migrations.AddField(
            model_name="board",
            name="title_pt_br",
            field=models.CharField(max_length=50, null=True, verbose_name="Title"),
        ),
        # Copia os valores originais para os novos campos pt_br
        migrations.RunPython(copy_original_to_translation_fields),
    ]
